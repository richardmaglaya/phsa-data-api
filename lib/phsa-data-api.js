/**
 * Created by richardmaglaya on 2017-10-11.
 */
'use strict';
var defaultDataVersion = 1;
var msSql = require('mssql');
var util = require('util');

var Connect = function() {
    var _this = exports;
};


//-- Local
var _checkErrorAndReturn = function (error, dbConnection, callback) {
	var _this = exports;
	_this.logger.error('ERROR-LOG: MsSQL Error [%j]', util.inspect(error));
	if (error instanceof Error && typeof error.message !== 'undefined' && error.message === 'no open connections') {
		dbConnection.close(function() {
			callback(error);
		});
	} else {
		callback(error);
	}
};


//-- Exposed function
var Insert = function () {
    var _this = exports;
    
    var args = arguments;
    var client = args[0] || {};
    var sqlInsertString = args[1] || {};
    var data = args[2] || {};
    var callback = args[4] || {};
    
    _this.logger.info('%j', { function: 'INFO-LOG: phsa-data-api.Insert() received arguments: ', args: args });
    if (!args.length) {
        throw new Error('Bad_Parameter: Expected JSON but received Null or Undefined');
    }
	
	msSql.connect(_this.mssqlServerConfig, function(error) {
		if (error) {
			_this.logger.error('%j', { function: 'ERROR-LOG: phsa-data-api.Insert.msSql.connect(error): ', error: error });
			callback(error, null);
		} else {
			var client = new msSql.Request();
			client.query(sqlInsertString, (error, result) => {
				if (error) {
					_this.logger.error('%j', { function: 'ERROR-LOG: phsa-data-api.Insert.msSql.Request(error): ', error: error });
					callback(error, null);
				} else {
					_this.logger.info('%j', { function: 'INFO-LOG: phsa-data-api.Insert.msSql.Request(result): ', result: result, rows: result.rows });
					//-- console.dir(result);
					callback(null, result);
				}
				
			});
		}
	});
	
	msSql.on('error', function(err) {
		_this.logger.error('%j', { function: 'ERROR-LOG: phsa-data-api.Insert.msSql.on(err): ', err: err });
	});
    
};


//-- Exposed function
var Select = function() {
    var _this = exports;
    
    var args = arguments;
    var client = args[0] || {};
    var sqlSelect = args[1] || {};
    var helper = args[2] || {};
    var callback = args[3] || {};
    
    _this.logger.info('%j', { function: 'INFO-LOG: phsa-data-api.Select() received arguments', sqlSelect: sqlSelect, helper: helper });
	
	const connPool = new msSql.ConnectionPool(_this.mssqlServerConfig, connectionPoolError => {
		if (connectionPoolError) {
			_this.logger.error('%j', {
				function: 'ERROR-LOG: phsa-data-api.Select.msSql.ConnectionPool(connectionPoolError): ',
				connectionPoolError: connectionPoolError
			});
			callback(connectionPoolError);
		} else {
			let clientConn = new msSql.Request(connPool);
			clientConn.query(sqlSelect, (queryError, result) => {
				
				if (queryError) {
					_this.logger.error('%j', {
						function: 'ERROR-LOG: phsa-data-api.Select.msSql.ConnectionPool(queryError): ',
						queryError: queryError
					});
					callback(connectionPoolError);
				} else {
					_this.logger.info('%j', {
						function: 'INFO-LOG: phsa-data-api.Select.msSql.Request(result): ',
						result: result
					});
					callback(null, result);
				}
				
				
				clientConn.on('error', clientConnError => {
					_this.logger.error('%j', {
						function: 'ERROR-LOG: phsa-data-api.Select.msSql.ConnectionPool(error): ',
						clientConnError: clientConnError
					});
				})
				
			});
		}
		
		connPool.on('error', err => {
			_this.logger.error('%j', {function: 'ERROR-LOG: phsa-data-api.Select.msSql.on(err): ', err: err});
			callback(err);
		});
	});
    
};


//-- Exposed function
var Update = function () {
    var _this = exports;
    
    var args = arguments;
    var sqlUpdateStr = args[0] || {};
    var callback = args[3] || {};
    
    _this.logger.info('%j', { function: 'INFO-LOG: phsa-data-api.Update() parsed arguments: ', sqlUpdate: sqlUpdate, updateParams: updateParams});
	
	const connPool = new msSql.ConnectionPool(_this.mssqlServerConfig, error => {
		if (error) {
			_this.logger.error('%j', { function: 'ERROR-LOG: phsa-data-api.Update.msSql.connect(error): ', error: error });
			callback(error, null);
		} else {
			var client = new msSql.Request(connPool);
			client.query(sqlUpdate, function (error, result) {
				if (error) {
					_this.logger.error('%j', { function: 'ERROR-LOG: phsa-data-api.Update.msSql.Request(error): ', error: error });
					callback(error, null);
				} else {
					_this.logger.info('%j', { function: 'INFO-LOG: phsa-data-api.Update.msSql.Request(result): ', result: result, rows: result.rows });
					callback(null, result);
				}
				
			});
		}
		
	})
	
	connPool.on('error', err => {
		_this.logger.error('%j', { function: 'ERROR-LOG: phsa-data-api.Update.msSql.on(err): ', err: err });
		callback(err, null);
	});
	
};


module.exports = function init(mssqlServerConfig, logger) {
    
    var _this = exports;
    
    _this.mssqlServerConfig = mssqlServerConfig;
    _this.logger = logger || console;
	
	_this.logger.info('%j', { function: 'INFO-LOG: phsa-data-api.init(mssqlServerConfig): ', mssqlServerConfig: _this.mssqlServerConfig });
	
	_this.backendHelpers = require('phsa-backend-helpers')();
    _this.enums = _this.backendHelpers.enums;
    _this.mssqlClientHelper = _this.backendHelpers.mssqlClientHelper(mssqlServerConfig);
    
    /**
     * NOTE to Developers (2017-10-11):
     *  - Keep the JSON variable names in ASC sort order
     * 	- Objects (JSON or Function) that are going to be exposed should start with upper-case varter and in CamelCase format (i.e. CollectionName)
     * 	- All non-DB constants should be all-upper-case and in underscore format
     * 	- Keep double-spacing in between `var` declarations for READABILITY
     */
    
    _this.insert = Insert;
    _this.update = Update;
    _this.select = Select;
    
    return _this;
    
    /**
     * NOTE to Developers (2017-10-11):
     *  - Keep the JSON variable names in ASC sort order
     * 	- Objects (JSON or Function) that are going to be exposed should start with upper-case varter and in CamelCase format (i.e. CollectionName)
     * 	- All non-DB constants should be all-upper-case and in underscore format
     * 	- Keep double-spacing in between `var` declarations for READABILITY
     */
    
};